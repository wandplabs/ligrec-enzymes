# ligrec-enzymes

A ligand-receptor list for cell-cell communication analysis including interactions mediated by small molecules, inferred by pairs of catabolic enzymes and cognate receptors.

## Description
Cell-cell gene interaction databses typically are built from documented protein-protein interactions. However, a large component of cell-cell communcation is mediated by small molecules which are not associated with gene names. By using the expression of enzymes which catalyze the final steps in the biosynthesis of such molecules to infer their presence, such interactions can be predicted between cell populations from RNA-seq data.

Pairs drawn from Guide2Pharma and DLRP in the original were removed; this list is meant as a supplement to other, more generic ones, and not a replacement.

## Installation
The list is provided as a `.csv` file in the [CellChatDB](https://github.com/sqjin/CellChat) format.

## Usage
The list can be appended to a CellChat list following their [tutorial](https://htmlpreview.github.io/?https://github.com/sqjin/CellChat/blob/master/tutorial/Update-CellChatDB.html).

```
library(CellChat)
interaction_input <- CellChatDB.human$interaction
interaction_input_add <- read.csv(file = "https://gitlab.com/wandplabs/ligrec-enzymes/-/raw/main/interaction_input_CellChatDB.csv", row.names = 1)
interaction_merged = rbind(interaction_input, interaction_input_add)

# construct the object
options(stringsAsFactors = FALSE)
CellChatDB <- list()
CellChatDB$interaction <- interaction_merged
CellChatDB$complex <- CellChatDB.human$complex
CellChatDB$cofactor <- CellChatDB.human$cofactor
CellChatDB$geneInfo <- CellChatDB.human$geneInfo

```

The list can be appended to a [Connectome](https://msraredon.github.io/Connectome/index.html) list as well.

```
library(Connectome)
# start with Raredon's connectome
interaction_input <- Connectome::ncomms8866_human
# slice off just the columns we want
interaction_input <- interaction_input[ , c('Ligand.ApprovedSymbol', 'Receptor.ApprovedSymbol', 'mode')]
interaction_input_add <- read.csv(file = "https://gitlab.com/wandplabs/ligrec-enzymes/-/raw/main/interaction_input_CellChatDB.csv", row.names = 1)
# subset to just the enzyme-mediated interactions
interaction_input_add <- subset(interaction_input_add, annotation == "Small Molecule-Mediated")
interaction_input_add <- interaction_input_add[ , c('ligand', 'receptor', 'pathway_name')]
colnames(interaction_input_add) <- c('Ligand.ApprovedSymbol', 'Receptor.ApprovedSymbol', 'mode')
rownames(interaction_input_add) <- NULL
interaction_merged = rbind(interaction_input, interaction_input_add)

connectome.genes <- union(interaction_merged$Ligand.ApprovedSymbol,interaction_merged$Receptor.ApprovedSymbol)
sc <- NormalizeData(sc)
genes <- connectome.genes[connectome.genes %in% rownames(sc)]
sc <- ScaleData(sc, features = genes)
con <- CreateConnectome(sc, LR.database = "custom", custom.list = interaction_merged, min.cells.per.ident = 10, p.values = F, calculate.DOR = F)
```

The list is also usable by other tools by extracting only the ligand and receptor pairs.

```
import omnipath as op
import pandas as pd

SOURCE = "source"
TARGET = "target"
curation_effort = 0 # minimum number of citations needed

# read in the omnipath database: can query the DB for CellPhoneDB/CellChatDB alone, or all DBs
intercell = op.interactions.import_intercell_network(
    include=['omnipath', 'pathwayextra', 'ligrecextra'], # define categories
    interactions_params={"databases": ["CellPhoneDB","CellChatDB"]}, # subset to just certain resources
)
# filter
intercell_filtered = intercell[
    (intercell['category_intercell_source'] == 'ligand') & # set transmitters to be ligands
    (intercell['category_intercell_target'] == 'receptor') & # set receivers to be receptors
    (intercell['curation_effort'] >= curation_effort) # must have at least x citations
]

# chip down to just source and target
intercell_filtered = intercell_filtered.loc[:,["genesymbol_intercell_source", "genesymbol_intercell_target"]]
intercell_filtered.columns = (SOURCE, TARGET)

# read in a custom l:r database
intercell_pavlicev2017 = pd.read_csv("https://gitlab.com/wandplabs/ligrec-enzymes/-/raw/main/interaction_input_CellChatDB.csv")
# subset to just the enzyme-related small molecule interactions (optional)
intercell_pavlicev2017[intercell_pavlicev2017.annotation == "Small Molecule-Mediated"]
intercell_pavlicev2017 = intercell_pavlicev2017.loc[:,["ligand", "receptor"]]
intercell_pavlicev2017.columns = (SOURCE, TARGET)

# concatenate the downloaded list and our custom list
intercell_merged = pd.concat([intercell_filtered, intercell_pavlicev2017], ignore_index=True).drop_duplicates()
```


## Roadmap
- check whether "Secreted Signaling" (non-enzyme) pairs are wanted or not
- consider adding "agonist" or "antagonist" information (e.g. HPGD for prostaglandins)?
- could use 2-protein complex handling to cover two-step synthesis pathways (e.g. PTGS2 + PTGES/2/3)?
- add citations
- upload to Ominpath

## Contributing
Contributions welcome, either following the existing database format, or as L:R pairs that can be processed before addition easily.

## Authors and acknowledgment
The list is a derivitave work of the list published by [Pavlicev, M., Wagner, G.P., Chavan, A.R., Owens, K., Maziarz, J., Dunn-Fletcher, C., Kallapur, S.G., Muglia, L., and Jones, H., 2017](https://dx.doi.org/10.1101/gr.207597.116), per the license of the original.

Additional curation by D. Stadtmauer

## License
The list is licensed as Creative Commons Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)
https://creativecommons.org/licenses/by-nc/4.0/

